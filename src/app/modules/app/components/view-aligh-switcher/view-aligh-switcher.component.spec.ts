import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAlighSwitcherComponent } from './view-aligh-switcher.component';

describe('ViewAlighSwitcherComponent', () => {
  let component: ViewAlighSwitcherComponent;
  let fixture: ComponentFixture<ViewAlighSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAlighSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAlighSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
