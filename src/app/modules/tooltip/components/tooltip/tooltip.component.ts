import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent {
  public message = '';
  @HostListener('click', ['$event']) onHostClick($event) {
    $event.stopPropagation();
  }
  constructor() {}
}
